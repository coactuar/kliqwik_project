<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100%;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="videoPlayer"></div>
<script src="js/jquery.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>

<script>
  var playerElement = document.getElementById("videoPlayer");

  var player = new Clappr.Player({
    source: 'https://cdn3.wowza.com/1/TnBNSk04N28yMFFV/WjhSZU9Y/hls/live/playlist.m3u8',
    plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              1: '480p', // 500kbps
              0: '360p', // 240kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        height: '100%',
    width: '100%',
    autoPlay: true,
    //poster: 'img/poster.jpg',
  });
  
  player.attachTo(playerElement);
  //player.play();
       
</script>
</body>
</html>