<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100%;
}
</style>

</head>

<body>
<!-- CSS -->
<link href="https://vjs.zencdn.net/7.2.3/video-js.css" rel="stylesheet">
<!-- HTML -->
<video id='videoPlayer' class="video-js vjs-default-skin" width="400" height="300" controls>
<!--<source type="application/x-mpegURL" src="https://d38233lepn6k1s.cloudfront.net/out/v1/f3e334c64f63448f9ed5a7465c47bc05/4553131ab95f4d748725f284186e6605/57c32c311a6e4f23bdd4bd4d63d426a1/index.m3u8">

<source type="application/x-mpegURL" src="https://d2tolx7e4cz3cw.cloudfront.net/demo/stream.m3u8">
-->
<source type="application/x-mpegURL" src="https://cdn3.wowza.com/1/TzYvcHhDR0M2c3A2/ZzJKUUs4/hls/live/playlist.m3u8">

</video>
<!-- JS code -->
<!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
<script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
<script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
<script>
var player = videojs('videoPlayer');
//player.play();
</script>
</body>
</html>